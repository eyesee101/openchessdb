package pl.org.openchessdb.service

import org.assertj.core.api.JUnitSoftAssertions
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import pl.org.openchessdb.repository.PlayerRepository
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@Transactional
@Rollback
class PlayerServiceTest {

    @Autowired
    lateinit var playerService: PlayerService

    @Autowired internal lateinit var playerRepository: PlayerRepository

    @get:Rule
    var softly = JUnitSoftAssertions()

    @Test
    fun addPlayer() {

        val name = UUID.randomUUID().toString().take(8)
        playerService.addPlayer(-1000, name)
        val find = playerRepository.findOneByFideId(-1000)!!
        softly.assertThat(find.name == name)
    }


}
