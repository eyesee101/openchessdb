package pl.org.openchessdb.repository.entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Parameter
import org.hibernate.id.enhanced.SequenceStyleGenerator
import pl.org.openchessdb.repository.enums.ChessResult
import javax.persistence.*
import javax.persistence.GenerationType.AUTO

@Entity
data class ChessGame (
    @GeneratedValue(strategy = AUTO, generator = "chessgame_id_seq")
    @GenericGenerator(name = "chessgame_id_seq", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = arrayOf(
                    Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "chessgame_id_seq"),
                    Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            ))
    @Id @Column val id: Long? = null,
    @ManyToOne @JoinColumn(nullable = false) val whitePlayer: Player = Player(),
    @ManyToOne @JoinColumn(nullable = false) val blackPlayerId: Player = Player(),
    @Column(nullable = false) val whitePlayerElo: Int = 0,
    @Column(nullable = false) val blackPlayerElo: Int = 0,
    @Column(nullable = false) @Enumerated val chessResult: ChessResult = ChessResult.UNSPECIFIED
)
