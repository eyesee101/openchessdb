package pl.org.openchessdb.repository.entity


import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Parameter
import org.hibernate.id.enhanced.SequenceStyleGenerator
import javax.persistence.*

@Entity
data class Player (
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "player_id_seq")
    @GenericGenerator(name = "player_id_seq", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = arrayOf(
                    Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "player_id_seq"),
                    Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            ))
    @Id @Column val id: Long? = null,
    @Column val fideId: Long = -1,
    @Column val name: String? = null
)
