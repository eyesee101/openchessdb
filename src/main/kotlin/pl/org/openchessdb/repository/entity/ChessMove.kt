package pl.org.openchessdb.repository.entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Parameter
import org.hibernate.id.enhanced.SequenceStyleGenerator
import javax.persistence.*
import javax.persistence.GenerationType.AUTO

@Entity
data class ChessMove (
    @GeneratedValue(strategy = AUTO, generator = "chessmove_id_seq")
    @GenericGenerator(name = "chessmove_id_seq", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = arrayOf(
                    Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "chessmove_id_seq"),
                    Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            ))
    @Id @Column val id: Long? = null,
    @ManyToOne @JoinColumn(nullable = false) val game: ChessGame = ChessGame(),
    @ManyToOne @JoinColumn(nullable = false) val currentPosition: ChessPosition = ChessPosition(),
    @ManyToOne @JoinColumn val nextPosition: ChessPosition? = null
)
