package pl.org.openchessdb.repository

import org.springframework.data.jpa.repository.JpaRepository
import pl.org.openchessdb.repository.entity.Player

internal interface PlayerRepository : JpaRepository<Player, Long> {
    fun findOneByFideId(fideId: Long): Player?
}