package pl.org.openchessdb.service

interface PlayerService {
    fun addPlayer(fideId: Long, name: String)
}