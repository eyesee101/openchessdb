package pl.org.openchessdb

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class OpenchessdbApplication

fun main(args: Array<String>) {
    SpringApplication.run(OpenchessdbApplication::class.java, *args)
}
