# Open Chess DB

[![Build Status](https://travis-ci.org/dmurgala/openchessdb.svg?branch=master)](https://travis-ci.org/dmurgala/openchessdb)

## Environment configuration

### PostgreSQL 10

- Requirements
    - Install [Docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/install/)
    - [docker-compose.yml](docker-compose.yml) contains database configuration for development

- Installation
    - First run will create database and volume *openchessdb-pgdata*
    - Run *docker-compose* in same directory as *docker-compose.yml*, otherwise use `-f ./docker/docker-compose.yml` 
      parameter
      
        ```bash
        cd docker
        ```
    - To run database in background
    
        ```bash
        docker-compose up -d
        ```
        
        ```
        Creating network "docker_default" with the default driver
        Creating openchessdb ... 
        Creating openchessdb ... done
        ```
        
    - To stop database
    
        ```bash
        docker-compose down
        ```
        
        ```bash
        Stopping openchessdb ... done
        Removing openchessdb ... done
        Removing network docker_default
        ```
        
    - To check logs (`-f` follows logs like `tail`)
    
        ```bash
        docker-compose logs -f
        ```
    
    - To manage volume *openchessdb-pgdata*:
        - inspect
        
        ```bash
        docker volume inspect openchessdb-pgdata
        ```
        
        - remove
        
        ```bash
        docker volume rm openchessdb-pgdata
        ```
           
### Running application
- To start
    - on Linux:    
    
      ```bash
      ./gradlew bootRun
      ````    

    - on Windows
    
      ```bash
      gradlew.bat bootRun
      ```
      
- Health ckeck
    
  ```bash
    curl http://localhost:8001/manage/health
  ```
  
  should return:
    `{"status":"UP"}`